import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const myIcon = ( <Icon name="rocket" size={30} color="#900" /> )

class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Profile',
  };
  render() {
    const { navigate } = this.props.navigation;
    const { name } = this.props.navigation.state.params
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <Image
            style={styles.image}
            source={{uri: 'https://placeimg.com/400/400/tech'}}
            />
          <Text style={styles.imageText}>{name}</Text>
        </View>
        <Button
          title="Go Back Home"
          onPress={() =>
            navigate( 'Home', { name: 'Jane' })
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'stretch',
    // backgroundColor: '#fafafa',
    // justifyContent: 'flex-start',
  },
  body: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
  },
  image: {
    width: 300,
    height: 300,
    // margin: 10,
    // padding: 10,
    borderWidth: 2,
    borderColor: 'black',
  },
  imageText: {
    textAlign: 'center',
    width: 300,
    padding: 10,
    borderWidth: 1,
    borderTopWidth: 0,
    borderColor: 'black',
  }
});


ProfileScreen.propTypes = {
  navigation: PropTypes.object
}

export default ProfileScreen
