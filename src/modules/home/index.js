import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, Button, SectionList, FlatList, ScrollView, RefreshControl } from 'react-native';

import { colors } from '../../styles/colors'

const profileInformation = [
  {
    key: 'Hey yo cool'
  }, {
    key: 'profile 2'
  }, {
    key: 'profile 3'
  }, {
    key: 'profile 4'
  }, {
    key: 'profile 5'
  }, {
    key: 'profile 6'
  }, {
    key: 'profile 7'
  }, {
    key: 'profile 8'
  }, {
    key: 'profile 9'
  }, {
    key: 'profile 10'
  }, {
    key: 'profile 11'
  }, {
    key: 'profile 12'
  }, {
    key: 'profile 13'
  }, {
    key: 'profile 14'
  }, {
    key: 'profile 15'
  }, {
    key: 'profile 16'
  }, {
    key: 'profile 17'
  }, {
    key: 'profile 18'
  }, {
    key: 'profile 19'
  }, {
    key: 'profile 20'
  }, {
    key: 'profile 21'
  }, {
    key: 'profile 22'
  }, {
    key: 'profile 23'
  }, {
    key: 'profile 24'
  }, {
    key: 'profile 25'
  }, {
    key: 'profile 26'
  }, {
    key: 'profile 27'
  }, {
    key: 'profile 28'
  }, {
    key: 'profile 29'
  }, {
    key: 'profile 30'
  }
]

const fetchData = () => new Promise( ( resolve ) => {
    setTimeout( () => resolve( true ), 1000 )
  })

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome'
  };

  constructor( props ) {
    super( props );
    this.state = {
      refreshing: false,
    };
  }

  _onRefresh() {
    this.setState({refreshing: true});
    fetchData().then( () => {
      this.setState({refreshing: false});
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind( this )}
        />
      }>
        <View style={styles.container}>
          <FlatList
            data={profileInformation}
            renderItem={({item}) => (
              <View style={styles.item}>
                <Button
                  title={item.key}
                  onPress={() =>
                    navigate( 'Profile', { name: item.key })
                  }
                  style={styles.button}
                />
              </View>
            )}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.gray_lighter_white,
  },
  button: {

  },
  item: {
    borderColor: colors.black,
    borderWidth: 0.5,
    padding: 10,
    margin: 10
  }
});



HomeScreen.propTypes = {
  navigation: PropTypes.object
}


export default HomeScreen
