export const colors = {
  // DEFAULT COLORS
  white:                 '#ffffff',
  black:                 '#000000',
  transparent:           'transparent',

  // GRAYS
  gray_lighter_white:    '#fafafa',
  gray_lighter:          '#eeeeee',
  gray_light_mid_mid:    '#e6e7e8',
  gray_light_mid:        '#dddddd',
  gray_light_primary:    '#c5c7cc',
  gray_light:            '#b3b3b3',
  gray_mid_light:        '#999999',
  gray_mid:              '#5f5f5f',
  gray:                  '#414042',
  gray_dark:             '#333333',
  gray_dark_blue:        '#32383d',
  gray_darker:           '#222222',
  gray_darker_black:     '#090909',

  gray_1:                '#111111',
  gray_2:                '#222222',
  gray_3:                '#333333',
  gray_4:                '#444444',
  gray_5:                '#555555',
  gray_6:                '#666666',
  gray_7:                '#777777',
  gray_8:                '#888888',
  gray_9:                '#999999',

  // CORE COLORS
  red:                   '#ff0000',
  green:                 '#00ff00',
  blue:                  '#0000ff',
  yellow:                '#ffff00',

  // UEG CORE
  disabled_color:        '#6f95a2',
  ueg_brown:             '#6b5d54',
  ueg_gray:              '#32383d',
  ueg_gray_light_mid:    '#cccccc',
  ueg_gray_light:        '#c5c7cc',
  ueg_border:            '1px solid ueg-gray-light-mid',

  // REDS
  ueg_red:               '#ec2f2d',
  ueg_dkred:             '#cc0200',
  ueg_altred:            '#532626',

  // GREENS
  ueg_green:             '#176a26',
  ueg_ltgreen:           '#30df50',
  ueg_altgreen:          '#25502d',

  // BLUES
  ueg_bgblue:            '#002b60',
  ueg_altblue:           '#337ab7',
  ueg_blue:              '#526f90',
  ueg_purple:            '#60217f',

  // YELLOWS
  ueg_yellow:            '#f0b823',
  ueg_orange:            '#ff9100',
}

export default colors
