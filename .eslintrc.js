module.exports = {
  "extends": [ "react-native" ],
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "rules": {
    "no-use-before-define": 0,
    "class-methods-use-this": 0,
    // "react/prefer-stateless-function": false,
    // "react-native/no-color-literals": 0,
    "react/jsx-no-bind": 0,
    "import/no-namespace": 0,
    "import/namespace": 1,
    "space-before-function-paren": [
      "error",
      {
        "anonymous": "never",
        "named": "never"
      }
    ],
    "space-in-parens": [
      "error",
      "always",
      {
        "exceptions": [
          "{}"
        ]
      }
    ],
    "spaced-comment": [
      "error",
      "always"
    ],
  }
}
