// import React from 'react';
// import Main from './src'

// export default class App extends React.Component { // eslint-disable-line react/prefer-stateless-function
//   render() {
//     return (
//       <Main />
//     );
//   }
// }

import React from 'react';
import HomeScreen from './src/modules/home'
import ProfileScreen from './src/modules/profile'

import {
  createStackNavigator,
} from 'react-navigation';

const App = createStackNavigator({
  Home: { screen: HomeScreen },
  Profile: { screen: ProfileScreen },
});

export default App
